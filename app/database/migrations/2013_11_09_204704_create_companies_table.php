<?php

use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration {

    public function up()
    {
        Schema::create('companies', function($table)
            {
                $table->increments('id');
                $table->timestamps();
                $table->string('name')->unique();
            });
    }

    public function down()
    {
        Schema::drop('companies');
    }

}