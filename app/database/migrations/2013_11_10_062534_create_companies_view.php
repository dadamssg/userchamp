<?php

use Illuminate\Database\Migrations\Migration;

class CreateCompaniesView extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE VIEW companiesView AS
                        select *,
                        (
                            SELECT group_concat(DISTINCT id separator ',')
                            FROM people as p
                            WHERE c.id = p.company_id
                        ) as person_ids
                        FROM companies as c");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW companiesView");
    }
}

//set group_concat_max_len=2048 - may need to up the size for group concats
