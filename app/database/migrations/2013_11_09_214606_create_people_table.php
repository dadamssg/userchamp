<?php

use Illuminate\Database\Migrations\Migration;

class CreatePeopleTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('people', function($table)
            {
                $table->increments('id');
                $table->timestamps();
                $table->string('email')->unique();
                $table->string('firstName');
                $table->string('lastName');
                $table->string('city');
                $table->string('state');
                $table->integer('company_id');
            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('people');
	}
}