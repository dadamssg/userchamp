<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

        $this->call('CompaniesSeeder');
        $this->command->info('Companies seeded!');

        $this->call('PeopleSeeder');
	 	$this->command->info('People seeded!');

        $this->call('TasksSeeder');
        $this->command->info('Tasks seeded!');
	}

}