<?php

use UserChamp\Models\Company;

class CompaniesSeeder extends Seeder {

    public function run()
    {
        DB::table('companies')->delete();

        Company::create(['name' => 'Acme Corp']);
        Company::create(['name' => 'Foo Bar Industries']);
        Company::create(['name' => 'Bogus Inc']);
    }
}