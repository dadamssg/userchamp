<?php

use UserChamp\Models\Person;

class PeopleSeeder extends Seeder
{

    public function run()
    {
        DB::table('people')->delete();

        $bob = [
            'firstName' => 'Bob',
            'lastName' => 'Johnson',
            'email' => 'bjohnson@gmail.com',
            'city' => 'Austin',
            'state' => 'TX',
            'company_id' => 1
        ];

        $allen = [
            'firstName' => 'Allen',
            'lastName' => 'Thompson',
            'email' => 'athomp@yahoo.com',
            'city' => 'San Diego',
            'state' => 'CA',
            'company_id' => 2
        ];


        $kara = [
            'firstName' => 'Kara',
            'lastName' => 'Smith',
            'email' => 'kparks@aol.com',
            'city' => 'Dallas',
            'state' => 'TX',
            'company_id' => 2
        ];

        $george = [
            'firstName' => 'George',
            'lastName' => 'Washington',
            'email' => 'gWash@hotmail.com',
            'city' => 'Washington',
            'state' => 'DC',
            'company_id' => 3
        ];

        Person::create($bob);
        Person::create($allen);
        Person::create($kara);
        Person::create($george);
    }
}