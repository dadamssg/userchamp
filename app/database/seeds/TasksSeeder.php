<?php
use UserChamp\Models\Person;
use UserChamp\Models\Company;

class TasksSeeder extends Seeder
{
    public function run()
    {
        DB::table('tasks')->delete();

        $faker = Faker\Factory::create();

        foreach (Person::all() as $person) {

            // Create however many tasks for this person
            foreach (range(1, rand(2, 4)) as $i){

                $person->tasks()->create(['subject' => $faker->sentence(rand(3,5))]);

                echo "created task for person ". $person->id.".";
            }
        }

        foreach (Company::all() as $company) {

            // Create however many tasks for this company
            foreach (range(1, rand(2, 4)) as $i){

                $company->tasks()->create(['subject' => $faker->sentence(rand(3,5))]);

                echo "Created task for person ". $company->id.".";
            }
        }
    }

}