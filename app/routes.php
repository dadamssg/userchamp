<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('register', array('as' => 'user.register', 'uses' => 'AuthController@register'));
Route::Post('register', array('as' => 'user.store', 'uses' => 'AuthController@store'));

Route::get(
    '/',
    function () {
        return View::make('hello');
    }
);

Route::get(
    'app',
    function () {

        return View::make('ember.index');
    }
);


Route::get(
    'test',
    function () {

        return "test";
    }
);

Route::resource('companies', 'CompaniesController');
Route::resource('people', 'PeopleController');
Route::resource('tasks', 'TasksController');
Route::get('people/{id}/tasks', 'TasksController@personTasks');
Route::get('companies/{id}/tasks', 'TasksController@companyTasks');
/*
Route::get('companies', function() {

        $json = new stdClass;

        $json->companies = [];

        $company1 = new stdClass;
        $company1->id = 1;
        $company1->name = "Abc Company";
        $company1->isCompleted = false;
        $company1->people = [1,2,3];

        $company2 = new stdClass;
        $company2->id = 2;
        $company2->name = "Acme Corp";
        $company2->isCompleted = true;
        $company2->people = [4,5];

        $json->companies[] = $company1;
        $json->companies[] = $company2;

        return Response::json($json);
});

Route::get('people', function() {

        $json = new stdClass;

        $bob = new stdClass;
        $bob->id = 6;
        $bob->firstName = "Bob";
        $bob->lastName = "Johnson";
        $bob->company_id = 1;
        $bob->city = "Bryan";
        $bob->state = "TX";

        $john = new stdClass;
        $john->id = 8;
        $john->firstName = "John";
        $john->lastName = "Smith";
        $john->company_id = 2;
        $john->city = "Los Angeles";
        $john->state = "CA";

        $people[] = $bob;
        $people[] = $john;

        $json->people = $people;

        return Response::json($json);
    });

/*
class Email
{
    protected $to;

    protected $subject;

    protected $message;

    public function setTo($to)
    {
        if (!is_string($to)) {
            throw new \InvalidArgumentException("The to address must be a string!");
        }

        $this->to = $to;
    }

    public function getTo()
    {
        return $this->to;
    }

    public function setSubject($subject)
    {
        if (!is_string($subject)) {
            throw new \InvalidArgumentException("The subject must be a string!");
        }

        $this->subject = $subject;
    }

    public function getSubject()
    {
        return $this->subject;
    }

    public function setMessage($message)
    {
        if (!is_string($message)) {
            throw new \InvalidArgumentException("The message must be a string!");
        }

        $this->message = $message;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function send()
    {
        return mail($this->getTo(), $this->getSubject(), $this->getMessage());
    }
} */

