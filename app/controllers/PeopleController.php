<?php

use UserChamp\Models\Person;

class PeopleController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $json = new stdClass;

        $q = \Input::get('q');

        if ($q) {

            $json->people = Person::where('firstName', 'like', "$q%")->get()->toArray();

        } else {

            $json->people = Person::all()->toArray();
        }

        foreach ($json->people as $key => $person) {

            $json->people[$key]['company'] = $person['company_id'];
            unset($json->people[$key]['company_id']);

            $json->people[$key]['links'] = [
                'tasks' => "/people/".$json->people[$key]['id']."/tasks"
               // 'tasks' => 'tasks'
            ];
        }

        return Response::json($json);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $saved = false;

        $error = 'No error';

        $input = \Input::json('person');

        $input['company_id'] = isset($input['company']) && $input['company'] ? $input['company'] : 0;

        $person = new Person($input);

        try {

            $saved = $person->save();

        } catch (\Exception $e) {

            $error = 'Error saving.';
        }

        if ($saved) {

            $json = new stdClass;

            $person->company = $person->company_id;

            $json->person = $person->toArray();

            return Response::json($json, 201);

        } else {

            return $this->respondWith($error, 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $json = new stdClass;

        $json->person = Person::find((int)$id)->toArray();

        return Response::json($json);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $input = \Input::json('person');

        $input['company_id'] = isset($input['company']) && $input['company'] ? $input['company'] : 0;

        $person = Person::find($id);

        if ($person) {

            $person->update($input);

            if ($person->save()) {

                $json = new stdClass;

                $person->company = $person->company_id;

                $json->person = $person->toArray();

                $json->person['company'] = $json->person['company_id'];
                unset($json->person['company_id']);

                $json->person['links'] = ['tasks' => "/people/".$json->person['id']."/tasks"];

                return Response::json($json, 200);
            }
        }

        return $this->respondWith("Error updating", 400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param $message
     * @param int $code
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWith($message, $code = 200)
    {
        $json = new stdClass;
        $json->code = $code;
        $json->message = $message;

        if ($code > 300) {

            $errors = new StdClass;
            $errors->error = [$message];
            $json->errors = $errors;
        }

        return Response::json($json, $code);
    }

}

