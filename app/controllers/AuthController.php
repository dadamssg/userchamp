<?php

use UserChamp\Validators\RegistrationValidator;
use UserChamp\Services\User\CreateUserService;

class AuthController extends \BaseController {

    protected $layout = 'layouts.master';

    /**
     * @var \ProgrammingAreHard\Data\Validators\UserValidator;
     */
    private $registrationForm;

    private $userService;

    public function __construct(RegistrationValidator $registrationForm, CreateUserService $userService)
    {
        $this->registrationForm = $registrationForm;

        $this->userService = $userService;
    }

    /**
     * Show the form for User registration.
     *
     * @return Response
     */
    public function register()
    {
        $this->layout->content = View::make('auth.signin');
    }

    /**
     * Store a newly created User.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();

        if ( ! $this->registrationForm->validate($input)) {

            $errors = $this->registrationForm->getErrors();

            return Redirect::route('user.register')->with('errors', $errors)->withInput();
        }

        $userData = $this->registrationForm->getUserData();

        $user = $this->userService->create($userData);

        //Auth::login($user);

        return "all good";
    }
}
