<?php

use UserChamp\Models\Company;
use UserChamp\Repositories\CompanyRepository;
use Illuminate\Database\Eloquent\Builder;

class CompaniesController extends \BaseController
{

    protected $repo;

    public function __construct(CompanyRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $res = new stdClass;

        $q = \Input::get('q');

        if ($q) {

            $res->companies = Company::where('name', 'like', "$q%")->get()->toArray();

        } else {

            $res->companies = Company::all()->toArray();
        }

        foreach ($res->companies as $key => $companies) {

            $res->companies[$key]['links'] = [
                'tasks' => "/companies/".$res->companies[$key]['id']."/tasks"
            ];
        }

        return Response::json($res);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $company = $this->repo->newInstance(\Input::all());

        if($this->repo->save($company)){

            $this->respondWith('Successfully created company.', 201);

        }

        $this->respondWith('Error', 400);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $company = Company::find((int)$id);

        if (is_null($company)) {

            return $this->respondWith('Company not found', 404);
        }

        $json = new stdClass;

        $json->company = Company::find((int)$id)->toArray();

        return Response::json($json);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $input = \Input::json('company');

        $company = Company::find($id);

        if ($company) {

            $company->update($input);

            if ($company->save()) {

                $json = new stdClass;

                $json->company = $company->toArray();

                $json->company['links'] = ['tasks' => "/companies/".$json->company['id']."/tasks"];

                return Response::json($json, 200);
            }
        }

        return $this->respondWith("Error updating", 400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param $message
     * @param int $code
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWith($message, $code = 200)
    {
        $json = new stdClass;
        $json->code = $code;
        $json->message = $message;

        return Response::json($json, $code);
    }

}