<?php

use UserChamp\Models\Task;

class TasksController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $json = new stdClass;

        $q = \Input::get('q');

        if ($q) {

            $json->tasks = Task::where('subject', 'like', "$q%")->get()->toArray();

        } else {

            $json->tasks = Task::all()->toArray();
        }

        foreach ($json->tasks as $key => $task) {

        }

        return Response::json($json);
    }

    public function personTasks($id)
    {
        $json = new stdClass;

        $json->tasks = Task::where('taskable_type', '=', 'Userchamp\Models\Person')
            ->where('taskable_id', '=', $id)
            ->get()
            ->toArray();

        foreach ($json->tasks as $key => $task) {

            $json->tasks[$key]['taskable_type'] = 'person';
        }

        return Response::json($json);
    }


    public function companyTasks($id)
    {
        $json = new stdClass;

        $json->tasks = Task::where('taskable_type', '=', 'Userchamp\Models\Company')
            ->where('taskable_id', '=', $id)
            ->get()
            ->toArray();

        foreach ($json->tasks as $key => $task) {

            $json->tasks[$key]['taskable_type'] = 'company';
        }

        return Response::json($json);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $saved = false;

        $error = 'No error';

        $input = \Input::json('task');

        $input = $this->sanitizeInput($input);

        $task = new Task($input);

        try {

            $saved = $task->save();

        } catch (\Exception $e) {

            $error = 'Error saving.';
        }

        if ($saved) {

            $json = new stdClass;

            $json->task = $this->prepareForOutput($task);

            return Response::json($json, 201);

        } else {

            return $this->respondWith($error, 400);
        }
    }

    protected function prepareForOutput(Task $task)
    {
        $taskable_type = strtolower($task->taskable_type);

        if (substr($taskable_type, -6) == 'person') {

            $task->taskable_type = 'person';

        } elseif (substr($taskable_type, -7) == 'company') {

            $task->taskable_type = 'company';
        }

        return $task->toArray();
    }

    protected function sanitizeInput(array $input)
    {
        $taskable_type = isset($input['taskable_type']) ? strtolower($input['taskable_type']) : '';

        if ($taskable_type == 'person') {

            $input['taskable_type'] = 'UserChamp\Models\Person';

        } elseif ($taskable_type == 'company') {

            $input['taskable_type'] = 'UserChamp\Models\Company';

        } else {

            $input['taskable_type'] = '';
        }

        return $input;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $json = new stdClass;

        $json->task = Task::find((int)$id)->toArray();

        return Response::json($json);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $input = \Input::json('task');

        $input = $this->sanitizeInput($input);

        $task = Task::find($id);

        if ($task) {

            $task->update($input);

            if ($task->save()) {

                $json = new stdClass;

                $taskable_type = strtolower($task->taskable_type);

                if (substr($taskable_type, -6) == 'person') {

                    $task->taskable_type = 'person';

                } elseif (substr($taskable_type, -7) == 'company') {

                    $task->taskable_type = 'company';
                }

                $json->task = $task->toArray();

                return Response::json($json, 200);
            }
        }

        return $this->respondWith("Error updating", 400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param $message
     * @param int $code
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWith($message, $code = 200)
    {
        $json = new stdClass;
        $json->code = $code;
        $json->message = $message;

        if ($code > 300) {

            $errors = new StdClass;
            $errors->error = [$message];
            $json->errors = $errors;
        }

        return Response::json($json, $code);
    }

}

