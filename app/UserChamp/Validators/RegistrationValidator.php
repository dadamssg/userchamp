<?php namespace UserChamp\Validators;

use ProgrammingAreHard\Data\Validators\UserValidator;
use ProgrammingAreHard\Data\Validators\BaseValidator;

class RegistrationValidator extends BaseValidator {

    /**
     * @var \ProgrammingAreHard\Data\Validators\UserValidator
     */
    protected $userValidation;

    /**
     * Inject the validation service and user validation rules
     *
     * @param \ProgrammingAreHard\Data\Validators\UserValidator $userValidation
     */
    public function __construct(UserValidator $userValidation)
    {
        $this->userValidation = $userValidation;
    }

    /**
     * Validate the data for the registration form
     *
     * @param array $input
     * @return bool
     */
    protected function validation(array $input)
    {
        if ( ! $this->userValidation->validateForCreate($input)) {

            $this->errors = array_merge($this->errors, $this->userValidation->getErrors());

            return false;
        }

        return true;
    }

    /**
     * Get only the user input provided
     *
     * @return array
     */
    public function getUserData()
    {
        return array_only($this->input, $this->userValidation->getFields());
    }
}