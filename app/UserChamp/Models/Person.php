<?php namespace UserChamp\Models;


class Person extends BaseModel {

    protected $table = 'people';

    protected $fillable = ['firstName', 'lastName', 'city', 'state', 'email','company_id'];

    public function company()
    {
        return $this->belongsTo('UserChamp\Models\Company');
    }

    public function tasks()
    {
        return $this->morphMany('UserChamp\Models\Task', 'taskable');
    }
} 