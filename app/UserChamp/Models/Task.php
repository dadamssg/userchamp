<?php namespace UserChamp\Models;


class Task extends BaseModel {

    protected $table = 'tasks';

    protected $fillable = ['subject', 'taskable_id', 'taskable_type'];

    public function taskable()
    {
        return $this->morphTo();
    }
} 