<?php namespace UserChamp\Models;

use Illuminate\Support\Facades\DB;

class Company extends BaseModel
{
    protected $table = 'companies';

    protected $readFrom = 'companiesView';

    protected $fillable = ['name'];

    protected $guarded = ['id'];

    protected $readOnly = ['person_ids'];

    public function people()
    {
        return $this->hasMany('UserChamp\Models\Person');
    }

    public function getPersonIdsAttribute($ids)
    {
        return $this->intArrayAttribute($ids);
    }

    public function tasks()
    {
        return $this->morphMany('UserChamp\Models\Task', 'taskable');
    }
} 