<?php namespace UserChamp;

use Illuminate\Support\ServiceProvider;

class UserChampServiceProvider extends ServiceProvider {

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function register()
    {
        //bind the the create user service
        $this->app->bind('UserChamp\Services\User\CreateUserService', function($app)
        {
            $hasher = $app->make('hash');

            $userRepo = $app->make('ProgrammingAreHard\Data\Repositories\UserRepositoryInterface');

            return new \UserChamp\Services\User\CreateUserService($userRepo, $hasher);
        });
    }
}