<?php namespace UserChamp\Services\API;

class Scope {

    protected $searchable = ['q'];

    protected $include = [];

    public function getIncluded()
    {
        return $this->include;
    }

    public function getSearchable()
    {
        return $this->searchable;
    }
} 