<?php namespace UserChamp\Services\User;

use ProgrammingAreHard\Data\Repositories\UserRepositoryInterface;
use Illuminate\Hashing\HasherInterface;

class UserService {

    protected $userRepo;

    protected $hasher;

    public function __construct(UserRepositoryInterface $userRepo, HasherInterface $hasher)
    {
        $this->userRepo = $userRepo;

        $this->hasher = $hasher;
    }

    public function prepareData(array $input)
    {
        if(isset($input['password'])) $input['password'] = $this->hasher->make($input['password']);

        return $input;
    }

    /**
     * Get the User repository
     *
     * @return \ProgrammingAreHard\Data\Repositories\UserRepositoryInterface
     */
    public function getUserRepo()
    {
        return $this->userRepo;
    }

}