<?php namespace UserChamp\Services\User;

class CreateUserService extends UserService {

    /**
     * Create the new User
     *
     * @param array $input
     * @return \ProgrammingAreHard\Data\Models\UserInterface
     */
    public function create(array $input)
    {
        $input = $this->prepareData($input);

        return $this->userRepo->create($input);
    }
}