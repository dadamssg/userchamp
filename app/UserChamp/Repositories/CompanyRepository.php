<?php namespace UserChamp\Repositories;

use UserChamp\Models\Company;

class CompanyRepository {

    /**
     * Save a company
     *
     * @param Company $model
     * @return bool
     * @throws \Exception
     */
    public function save(Company $model)
    {
        return $model->save();
    }

    /**
     * Get a new company instance
     *
     * @param array $attributes
     * @return Company
     */
    public function newInstance(array $attributes = array())
    {
        return new Company($attributes);
    }
} 