<?php namespace UserChamp\Repositories;

use Cartalyst\Sentry\Sentry;

class SentryEloquentUserRepository implements UserRepository{

    private $sentry;

    public function __construct(Sentry $sentry)
    {
        $this->sentry = $sentry;
    }

    public function create(array $input)
    {
        return $this->sentry->createUser($input);
    }
}