<?php namespace UserChamp\Repositories;


interface UserRepository {

    public function create(array $input);

    public function findById($id);
}

