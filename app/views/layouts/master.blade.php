<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="David Adams">

    <title>ProgrammingAreHard.com</title>

    <link type="text/css" rel="stylesheet" href="../../css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="../../css/bootstrap-theme.min.css">

    @section('css')
    @show
</head>

<body>
@section('navbar')
@show
<div class="container">

    @yield('content')

</div> <!-- /container -->

<script src="../../js/bootstrap.min.js"></script>
</body>
</html>
