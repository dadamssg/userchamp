@extends('layouts.master')

@section('css')
    <link type="text/css" rel="stylesheet" href="../../css/signin.css">
@stop

@section('content')
    <form method="POST" action="{{ URL::route('user.store') }}" class="form-horizontal sign-in-form" role="form">

        @if (Session::has('errors'))
        <ul class='errors'>
            @foreach (Session::get('errors') as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
        @endif

        {{ Form::token() }}
        <div class="form-group">
            <div class="col-lg-12">
                {{ Form::email('email', null, ['id' => 'inputEmail1', 'class' => 'form-control', 'placeholder' => 'Email']) }}
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-12">
                {{ Form::password('password', ['id' => 'inputPassword1', 'class' => 'form-control', 'placeholder' => 'Password']) }}
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-12">
                {{ Form::password('password_confirmation', ['id' => 'inputPassword2', 'class' => 'form-control', 'placeholder' => 'Confirm Password']) }}
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-12">
                <button type="submit" class="btn btn-primary">Sign in</button>
            </div>
        </div>
    </form>
@stop