<?php

use Illuminate\Validation\Validator as Validator;
use ProgrammingAreHard\Data\Validators\UserValidator;
use ProgrammingAreHard\Data\Models\UserInterface;
use ProgrammingAreHard\Data\Models\Eloquent\User;
use Mockery as m;

class UserValidatorTest extends PHPUnit_Framework_TestCase {

    protected $laravelValidator;

    protected $userValidator;

    protected $user;

    public function setUp()
    {
        $this->laravelValidator = m::mock('Illuminate\Validation\Validator');

        $this->user = m::mock('ProgrammingAreHard\Data\Models\Eloquent\User');

        $this->userValidator = new UserValidator($this->laravelValidator);
    }

    /**
     * Close mockery.
     *
     * @return void
     */
    public function tearDown()
    {
        m::close();
    }

    public function testValidatorPassesRulesAndDataToLaravelValidator()
    {
        $data = ['email' => 'johndoe@gmail.com', 'password' => 'testing', 'password_confirmation' => 'testing'];

        $rulesForCreate = [
            'email'    => 'required|email|unique:users',
            'password' => 'required|confirmed'
        ];

        $this->laravelValidator->shouldReceive('setRules')->once()->with($rulesForCreate);

        $this->laravelValidator->shouldReceive('setData')->once()->with($data);

        $this->laravelValidator->shouldReceive('passes')->andReturn(true);

        $this->assertTrue($this->userValidator->validateForCreate($data));
    }
}