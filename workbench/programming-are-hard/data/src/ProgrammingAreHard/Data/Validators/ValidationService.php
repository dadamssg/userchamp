<?php namespace ProgrammingAreHard\Data\Validators;

use Illuminate\Validation\Validator;

class ValidationService implements ValidatorInterface {

    protected $validator;

    protected $errors = [];

    protected $rules = [];

    public function __construct(Validator $validator)
    {
        $this->validator = $validator;
    }

    public function setRules(array $rules)
    {
        $this->rules = $rules;
    }

    public function addRules(array $rules)
    {
        $this->rules = array_merge($this->rules, $rules);
    }

    public function validate(array $data)
    {
        $this->errors = [];

        $this->validator->setRules($this->rules);

        $this->validator->setData($data);

        if ($this->validator->passes()) {

            return true;
        }

        $this->errors = $this->validator->messages()->all();

        return false;
    }

    public function getErrors()
    {
        return $this->errors;
    }
}