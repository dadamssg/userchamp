<?php namespace ProgrammingAreHard\Data\Validators;

interface HasModelRules {

    /**
     * Get the validation rules for the model when creating
     *
     * @return array
     */
    public function getRulesForCreate();

    /**
     * Get the validation rules for the model when updating
     *
     * @return array
     */
    public function getRulesForUpdate();

    /**
     * Get all fields that can be persisted
     *
     * @return array
     */
    public function getFields();
}