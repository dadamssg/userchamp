<?php namespace ProgrammingAreHard\Data\Validators;


abstract class BaseValidator implements ValidatorInterface{

    /**
     * All input being validated
     *
     * @var array
     */
    protected $input;

    /**
     * All errors generated from the validation attempt
     *
     * @var array
     */
    protected $errors = [];

    /**
     * Get all errors from the validation attempt
     *
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Validate the incoming data
     *
     * @param array $input
     * @return bool
     */
    public function validate(array $input)
    {
        $this->errors = [];

        $this->input  = $input;

        return $this->validation($input);
    }

    /**
     * Custom validation
     *
     * @param array $input
     * @return bool
     */
    abstract protected function validation(array $input);
}