<?php namespace ProgrammingAreHard\Data\Validators;

interface ValidatorInterface {


    /**
     * @param array $input
     * @return bool
     */
    public function validate(array $input);

    /**
     * Get the errors from the validation attempt
     *
     * @return array
     */
    public function getErrors();
}