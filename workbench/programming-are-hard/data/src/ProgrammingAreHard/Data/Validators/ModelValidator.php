<?php namespace ProgrammingAreHard\Data\Validators;

use Illuminate\Validation\Validator;
use ProgrammingAreHard\Data\Validators\HasModelRules;

abstract class ModelValidator implements HasModelRules {

    /**
     * @var Validator
     */
    private $validator;

    /**
     * Errors generated from the validation attempt
     *
     * @var array
     */
    protected $errors = [];

    /**
     * Inject the Laravel Validator
     *
     * @param Validator $validator
     */
    public function __construct(Validator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Validate some data accoring to rules for creating a model
     *
     * @param array $data
     * @return bool
     */
    public function validateForCreate(array $data)
    {
        return $this->validate($this->getRulesForCreate(), $data);
    }

    /**
     * Validate some data according to rules for updating a model
     *
     * @param array $data
     * @return bool
     */
    public function validateForUpdate(array $data)
    {
        return $this->validate($this->getRulesForUpdate(), $data);
    }

    /**
     * Validate some data according to some Laravel validation rules
     *
     * @param array $rules
     * @param array $data
     * @return bool
     */
    private function validate(array $rules, array $data)
    {
        $this->errors = [];

        $this->validator->setRules($rules);

        $this->validator->setData($data);

        if ($this->validator->passes()) {

            return true;
        }

        $this->errors = $this->validator->messages()->all();

        return false;
    }

    /**
     * Get the errors from the validation attempt
     *
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }
}