<?php namespace ProgrammingAreHard\Data\Validators;

class UserValidator extends ModelValidator {

    /**
     * Laravel validation ruleset for model creation
     *
     * @var array
     */
    protected $rulesForCreate = [
        'email'    => 'required|email|unique:users',
        'password' => 'required|confirmed'
    ];

    /**
     * Laravel validation ruleset for updating model
     *
     * @var array
     */
    protected $rulesForUpdate = [
        'email'    => 'required|email|unique:users',
    ];
    /**
     * Get the Eloquent validation rules for the model
     *
     * @return array
     */
    public function getRulesForCreate()
    {
        return $this->rulesForCreate;
    }

    /**
     * Get the validation rules for the model when updating
     *
     * @return array
     */
    public function getRulesForUpdate()
    {
        return $this->rulesForUpdate;
    }

    /**
     * Ignore this User id when checking uniqueness
     *
     * @param int $id
     * @return void
     */
    public function ignoreUserId($id)
    {
        $this->rulesForUpdate['email'] = "required|email|unique:users,email,$id";
    }

    /**
     * Get all the user field names
     *
     * @return array
     */
    public function getFields()
    {
        return ['email', 'password', 'first_name', 'last_name'];
    }
}