<?php namespace ProgrammingAreHard\Data;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class DataServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('programming-are-hard/data');

        $this->app->singleton('ProgrammingAreHard\Data\Repositories\UserRepositoryInterface', 'ProgrammingAreHard\Data\Repositories\Eloquent\UserRepository');

        //bind the user validator
        $this->app->bind('ProgrammingAreHard\Data\Validators\UserValidator', function($app){

            $validator = Validator::make([],[]);

            return new \ProgrammingAreHard\Data\Validators\UserValidator($validator);
        });
    }

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}