<?php namespace ProgrammingAreHard\Data\Repositories;

use ProgrammingAreHard\Data\Models\UserInterface;

interface UserRepositoryInterface {

    /**
     * Get a new User instance
     *
     * @return \ProgrammingAreHard\Data\Models\UserInterface
     */
    public function newUser();

    /**
     * Inflate a User with data
     *
     * @param array $input
     * @return \ProgrammingAreHard\Data\Models\UserInterface
     */
    public function create(array $input);

    /**
     * Retrieve the current User
     *
     * @return \ProgrammingAreHard\Data\Models\UserInterface
     */
    public function currentUser();

    /**
     * Find a User by id
     *
     * @param $id int
     * @return \ProgrammingAreHard\Data\Models\UserInterface
     */
    public function findById($id);

    /**
     * Persist a User
     *
     * @param UserInterface $user
     * @return bool
     */
    public function save(UserInterface $user);
}