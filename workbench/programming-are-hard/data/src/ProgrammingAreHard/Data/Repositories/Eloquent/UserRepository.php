<?php namespace ProgrammingAreHard\Data\Repositories\Eloquent;

use ProgrammingAreHard\Data\Models\UserInterface;
use ProgrammingAreHard\Data\Repositories\UserRepositoryInterface;
use ProgrammingAreHard\Data\Models\Eloquent\User;
use Illuminate\Support\Facades\Auth;

class UserRepository implements UserRepositoryInterface {

    /**
     * Get a new User instance
     *
     * @return \ProgrammingAreHard\Data\Models\UserInterface
     */
    public function newUser()
    {
        return new User();
    }
    /**
     * Inflate a User with data
     *
     * @param array $input
     * @return \ProgrammingAreHard\Data\Models\UserInterface
     */
    public function create(array $input)
    {
        $user = $this->newUser();

        foreach ($input as $field => $value) {

            $user->$field = $value;
        }

        $user->save();

        return $user;
    }

    /**
     * Retrieve the current User
     *
     * @return \ProgrammingAreHard\Data\Models\UserInterface
     */
    public function currentUser()
    {
        return Auth::user();
    }

    /**
     * Find a User by id
     *
     * @param $id int
     * @return \ProgrammingAreHard\Data\Models\UserInterface
     */
    public function findById($id)
    {
        // TODO: Implement findById() method.
    }

    /**
     * Persist a User
     *
     * @param UserInterface $model
     * @return bool
     */
    public function save(UserInterface $user)
    {
        return $user->save();
    }


}