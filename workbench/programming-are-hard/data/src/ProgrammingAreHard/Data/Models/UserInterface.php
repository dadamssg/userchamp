<?php namespace ProgrammingAreHard\Data\Models;


interface UserInterface {

    /**
     * Set the email field
     *
     * @param string $email
     * @return void
     */
    public function setEmail($email);

    /**
     * Get the email field
     *
     * @return string $email
     */
    public function getEmail();

    /**
     * Set the raw password
     *
     * @param string $password
     * @return void
     */
    public function setPassword($password);

    /**
     * Get the password
     *
     * @return string
     */
    public function getPassword();
}